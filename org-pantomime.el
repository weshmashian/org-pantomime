;;; org-pantomime.el --- org html export for text/html MIME emails based on org-mime.el

;; org-mime.el Copyright (C) 2010-2015 Eric Schulte
;; org-pantomime.el Copyright (C) 2017, 2018 Marko Crnić

;; Author: Eric Schulte
;; Maintainer: Marko Crnić (weshmashian)
;; Keywords: mime, mail, email, html
;; Homepage: https://gitlab.com/weshmashian/org-pantomime
;; Version: 0.1.0
;; Package-Requires: ((cl-lib "0.5"))

;; This file is not part of GNU Emacs.

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; WYSWYG, html mime composition using org-mode
;;
;; For mail composed using the orgstruct-mode minor mode, this
;; provides a function for converting all or part of your mail buffer
;; to embedded html as exported by org-mode.  Call `org-pantomime-htmlize'
;; in a message buffer to convert either the active region or the
;; entire buffer to html.
;;
;; Similarly the `org-pantomime-org-buffer-htmlize' function can be called
;; from within an org-mode buffer to convert the buffer to html, and
;; package the results into an email handling with appropriate MIME
;; encoding.
;;
;; you might want to bind this to a key with something like the
;; following message-mode binding
;;
;;   (add-hook 'message-mode-hook
;;             (lambda ()
;;               (local-set-key "\C-c\M-o" 'org-pantomime-htmlize)))
;;
;; and the following org-mode binding
;;
;;   (add-hook 'org-mode-hook
;;             (lambda ()
;;               (local-set-key "\C-c\M-o" 'org-pantomime-org-buffer-htmlize)))

;;; Code:
(require 'cl-lib)

(declare-function org-export-string-as "ox"
		  (string backend &optional body-only ext-plist))
(declare-function org-export-get-tags "ox" (element info &optional tags inherited))
(declare-function org-export-get-environment "ox" (&optional backend subtreep ext-plist))
(declare-function org-trim "org" (s &optional keep-lead))
(declare-function org-end-of-meta-data "org" (&optional full))
(declare-function org-narrow-to-subtree "org" ())
(declare-function org-entry-get "org"
                  (pom property &optional inherit literal-nil))
(declare-function org-heading-components "org" ())
(declare-function org-region-active-p "org-compat" ())
(declare-function org-element-map "org-element"
                  (data types fun &optional info first-match no-recursion with-affiliated))
(declare-function org-element-parse-buffer "org-element" (&optional granularity visible-only))
(declare-function org-element-property "org-element" (property element))
(declare-function message-goto-body "message" ())

(defgroup org-pantomime nil
  "WYSWYG, HTML MIME composition using org-mode."
  :group 'org)

(defcustom org-pantomime-use-property-inheritance nil
  "Non-nil means al MAIL_ properties apply also for sublevels."
  :group 'org-pantomime
  :type 'boolean)

(defcustom org-pantomime-default-header
  "#+OPTIONS: latex:t\n"
  "Default header to control html export options, and ensure
  first line isn't assumed to be a title line."
  :group 'org-pantomime
  :type 'string)

(defcustom org-pantomime-library 'mml
  "Library to use for marking up MIME elements."
  :group 'org-pantomime
  :type '(choice 'mml 'semi 'vm))

(defcustom org-pantomime-preserve-breaks t
  "Used as temporary value of `org-export-preserve-breaks' during
  mime encoding."
  :group 'org-pantomime
  :type 'boolean)

(defcustom org-pantomime-fixedwith-wrap
  "<pre style=\"font-family: courier, monospace;\">\n%s</pre>\n"
  "Format string used to wrap a fixedwidth HTML email."
  :group 'org-pantomime
  :type 'string)

(defcustom org-pantomime-html-hook nil
  "Hook to run over the html buffer before attachment to email.
  This could be used for example to post-process html elements."
  :group 'org-pantomime
  :type 'hook)

(dolist (fmt '("ascii" "org" "html"))
  (eval `(defcustom
           ,(intern (concat "org-pantomime-pre-" fmt "-hook"))
           nil
           (concat "Hook to run before " fmt " export.\nFunctions "
                   "should take no arguments and will be run in a "
                   "buffer holding\nthe text to be exported."))))

(defcustom org-pantomime-send-subtree-hook nil
  "Hook to run in the subtree in the Org-mode file before export."
  :group 'org-pantomime
  :type 'hook)

(defcustom org-pantomime-send-buffer-hook nil
  "Hook to run in the Org-mode file before export."
  :group 'org-pantomime
  :type 'hook)

(defcustom org-pantomime-send-buffer-backend 'html
  "Org export backend to use when exporting a buffer."
  :group 'org-pantomime
  :type 'sexp)

(defcustom org-pantomime-send-subtree-backend 'org
  "Org export backend to use when exporting a subtree.

This value can be overriden with MAIL_FMT Org property."
  :group 'org-pantomime
  :type 'sexp)

(defcustom org-pantomime-latex-processor 'dvipng
  "Used as temporary value of `org-html-with-latex' during mime encoding."
  :group 'org-pantomime
  :type 'sexp)

;; example hook, for setting a dark background in <pre style="background-color: #EEE;"> elements
(defun org-pantomime-change-element-style (element style)
  "Set new default htlm style for <ELEMENT> elements in exported html."
  (while (re-search-forward (format "<%s\\>" element) nil t)
    (replace-match (format "<%s style=\"%s\"" element style))))

(defun org-pantomime-change-class-style (class style)
  "Set new default htlm style for objects with classs=CLASS in
exported html."
  (while (re-search-forward (format "class=\"%s\"" class) nil t)
    (replace-match (format "class=\"%s\" style=\"%s\"" class style))))

;; ;; example addition to `org-pantomime-html-hook' adding a dark background
;; ;; color to <pre> elements
;; (add-hook 'org-pantomime-html-hook
;;           (lambda ()
;;             (org-pantomime-change-element-style
;;              "pre" (format "color: %s; background-color: %s;"
;;                            "#E6E1DC" "#232323"))
;; 	    (org-pantomime-change-class-style
;;              "verse" "border-left: 2px solid gray; padding-left: 4px;")))

(defun org-pantomime-file (ext path id)
  "Markup a file for attachment."
  (cl-case org-pantomime-library
    ('mml (format (concat "<#part type=\"%s\" filename=\"%s\" "
			  "disposition=inline id=\"<%s>\">\n<#/part>\n")
		  ext path id))
    ('semi (concat
            (format (concat "--[[%s\nContent-Disposition: "
			    "inline;\nContent-ID: <%s>][base64]]\n")
		    ext id)
            (base64-encode-string
             (with-temp-buffer
               (set-buffer-multibyte nil)
               (insert-file-contents-literally path)
               (buffer-string)))))
    ('vm "?")))

(defun org-pantomime-attach-file (file)
  "Add FILE as attachment to composed mail."
  (cl-case org-pantomime-library
    ('mml (unless (org-pantomime-file-attached-p file)
            (mml-attach-file file)))
    ('semi "?")
    ('vm "?")))

(defun org-pantomime-file-attached-p (file)
  "Check if FILE is already attached."
  (save-excursion
    (goto-char (point-max))
    (re-search-backward (concat "^<#part .*filename=\"" file "\"") nil t)))

(defun org-pantomime-multipart (plain html &optional images)
  "Markup a multipart/alternative with text/plain and text/html alternatives.
If the html portion of the message includes images wrap the html
and images in a multipart/related part."
  (cl-case org-pantomime-library
    ('mml (concat "<#multipart type=alternative><#part type=text/plain>"
		  plain
		  (when images "<#multipart type=related>")
		  "<#part type=text/html>"
		  html
		  images
		  (when images "<#/multipart>\n")
		  "<#/multipart>\n"))
    ('semi (concat
            "--" "<<alternative>>-{\n"
            "--" "[[text/plain]]\n" plain
	    (if (and images (> (length images) 0))
		(concat "--" "<<related>>-{\n"
			"--" "[[text/html]]\n"  html
			images
			"--" "}-<<related>>\n")
	      (concat "--" "[[text/html]]\n"  html
		      images))
            "--" "}-<<alternative>>\n"))
    ('vm "?")))

(defun org-pantomime-replace-images (str)
  "Replace images in html files with cid links."
  (let (html-images)
    (cons
     (replace-regexp-in-string ;; replace images in html
      "src=\"\\([^\"]+\\)\""
      (lambda (text)
        (format
         "src=\"cid:%s\""
         (let* ((url (and (string-match "src=\"\\(?:file://\\)?\\([^\"]+\\)\"" text)
                          (match-string 1 text)))
                (path (expand-file-name url nil))
                (ext (file-name-extension path))
                (id (replace-regexp-in-string "[\/\\\\]" "_" path)))
           (add-to-list 'html-images
                        (org-pantomime-file (concat "image/" ext) path id))
           id)))
      str)
     html-images)))

;;;###autoload
(defun org-pantomime-htmlize (&optional arg)
  "Export to HTML an email body composed using `mml-mode'.
If called with an active region only export that region,
otherwise export the entire body."
  (interactive "P")
  (require 'ox-org)
  (require 'ox-html)
  (let* ((region-p (org-region-active-p))
         (html-start (or (and region-p (region-beginning))
                         (save-excursion
                           (goto-char (point-min))
                           (search-forward mail-header-separator)
                           (+ (point) 1))))
         (html-end (or (and region-p (region-end))
                       ;; TODO: should catch signature...
                       (point-max)))
         (raw-body (concat org-pantomime-default-header
			   (buffer-substring html-start html-end)))
         (body (org-export-string-as raw-body 'org t))
         ;; because we probably don't want to export a huge style file
         (org-export-htmlize-output-type 'inline-css)
         ;; makes the replies with ">"s look nicer
         (org-export-preserve-breaks org-pantomime-preserve-breaks)
	 ;; dvipng for inline latex because MathJax doesn't work in mail
	 (org-html-with-latex org-pantomime-latex-processor)
         ;; to hold attachments for inline html images
         (html-and-images
          (org-pantomime-replace-images
	   (org-export-string-as raw-body 'html t)))
         (html-images (unless arg (cdr html-and-images)))
         (html (org-pantomime-apply-html-hook
                (if arg
                    (format org-pantomime-fixedwith-wrap body)
                  (car html-and-images)))))
    (delete-region html-start html-end)
    (save-excursion
      (goto-char html-start)
      (insert (org-pantomime-multipart
	       body html (mapconcat 'identity html-images "\n"))))))

(defun org-pantomime-apply-html-hook (html)
  (if org-pantomime-html-hook
      (with-temp-buffer
        (insert html)
        (goto-char (point-min))
        (run-hooks 'org-pantomime-html-hook)
        (buffer-string))
    html))

(defmacro org-pantomime-try (&rest body)
  `(condition-case nil ,@body (error nil)))

(defun org-pantomime-send-subtree (&optional fmt)
  (save-restriction
    (org-narrow-to-subtree)
    (run-hooks 'org-pantomime-send-subtree-hook)
    (let* ((mp (lambda (p) (org-entry-get nil p org-pantomime-use-property-inheritance)))
	   (file (buffer-file-name (current-buffer)))
	   (subject (or (funcall mp "MAIL_SUBJECT") (nth 4 (org-heading-components))))
	   (to (funcall mp "MAIL_TO"))
	   (cc (funcall mp "MAIL_CC"))
	   (bcc (funcall mp "MAIL_BCC"))
	   (from (funcall mp "MAIL_FROM"))
	   (body (buffer-substring
                  (save-excursion (goto-char (point-min))
                                  (org-end-of-meta-data t)
                                  (point))
		  (point-max))))
      (org-pantomime-compose body (or fmt 'org) file to subject
			`((cc . ,cc) (bcc . ,bcc) (from . ,from))))))

(defun org-pantomime-send-buffer (&optional fmt)
  (run-hooks 'org-pantomime-send-buffer-hook)
  (let* ((region-p (org-region-active-p))
	 (file (buffer-file-name (current-buffer)))
	 (subject (if (not file) (buffer-name (buffer-base-buffer))
		   (file-name-sans-extension
		    (file-name-nondirectory file))))
         (body-start (or (and region-p (region-beginning))
                         (save-excursion (goto-char (point-min)))))
         (body-end (or (and region-p (region-end)) (point-max)))
	 (temp-body-file (make-temp-file "org-pantomime-export"))
	 (body (buffer-substring body-start body-end)))
    (org-pantomime-compose body (or fmt 'org) file nil subject)))

(defun org-pantomime--get-files ()
  "Returns a list of paths referenced in file links.

Respects export excludes."
  (let (filelist)
    (org-element-map (org-element-parse-buffer) 'link
      (lambda (link)
        (when (string= (org-element-property :type link) "file")
          (let* ((taglist (org-export-get-tags link nil nil 'inherited))
                 (linktype (org-element-property :type link))
                 (linkpath (org-element-property :path link))
                 (truepath (file-truename linkpath))
                 (excludetags (plist-get (org-export-get-environment) :exclude-tags)))
            (cond
             ((null excludetags) (setq filelist (cons truepath filelist)))
             (t (dolist (tag excludetags)
                  (unless (member tag taglist)
                    (setq filelist (cons truepath filelist))))))))))
    filelist))

(defun org-pantomime-compose (body fmt file &optional to subject headers)
  (require 'message)
  (let ((bhook
	 (lambda (body fmt)
	   (let ((hook (intern (concat "org-pantomime-pre-"
				       (symbol-name fmt)
				       "-hook"))))
	     (if (> (eval `(length ,hook)) 0)
		 (with-temp-buffer
		   (insert body)
		   (goto-char (point-min))
		   (eval `(run-hooks ',hook))
		   (buffer-string))
	       body))))
	(fmt (if (symbolp fmt) fmt (intern fmt)))
        (files (org-pantomime--get-files)))
    (compose-mail to subject headers nil)
    (message-goto-body)
    (cond
     ((eq fmt 'org)
      (require 'ox-org)
      (insert (org-export-string-as
	       (org-trim (funcall bhook body 'org)) 'org t)))
     ((eq fmt 'ascii)
      (require 'ox-ascii)
      (insert (org-export-string-as
	       (concat "#+Title:\n" (funcall bhook body 'ascii)) 'ascii t)))
     ((or (eq fmt 'html) (eq fmt 'html-ascii))
      (require 'ox-ascii)
      (require 'ox-org)
      (let* ((org-link-file-path-type 'absolute)
	     ;; we probably don't want to export a huge style file
	     (org-export-htmlize-output-type 'inline-css)
             (org-html-with-latex org-pantomime-latex-processor)
	     (html-and-images
	      (org-pantomime-replace-images
	       (org-export-string-as (funcall bhook body 'html) 'html t)))
	     (images (cdr html-and-images))
	     (html (org-pantomime-apply-html-hook (car html-and-images))))
	(insert (org-pantomime-multipart
		 (org-export-string-as
		  (org-trim
		   (funcall bhook body (if (eq fmt 'html) 'org 'ascii)))
		  (if (eq fmt 'html) 'org 'ascii) t)
		 html)
		(mapconcat 'identity images "\n")))))
    (mapc 'org-pantomime-attach-file files)))

;;;###autoload
(defun org-pantomime-org-buffer-htmlize ()
  "Create an email buffer containing the current org-mode file
  exported to html and encoded in both html and in org formats as
  mime alternatives."
  (interactive)
  (org-pantomime-send-buffer org-pantomime-send-buffer-backend))

;;;###autoload
(defun org-pantomime-subtree ()
  "Create an email buffer containing the current org-mode subtree
  exported to a org format or to the format specified by the
  MAIL_FMT property of the subtree."
  (interactive)
  (org-pantomime-send-subtree
   (or (org-entry-get nil "MAIL_FMT" org-pantomime-use-property-inheritance) org-pantomime-send-subtree-backend)))

(provide 'org-pantomime)
;;; org-pantomime.el ends here
